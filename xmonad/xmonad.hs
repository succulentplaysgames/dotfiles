import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import Data.Monoid
import System.Exit
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Hooks.ManageDocks

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

myTerminal      = "kitty"				-- Terminal
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True				-- Hover focuses
myClickJustFocuses :: Bool
myClickJustFocuses = False				-- Clicking focuses
myBorderWidth   = 1					-- Borderwidth
myModMask       = mod1Mask				-- Modmask

xmobarEscape = concatMap doubleLts
  where doubleLts '<' = "<<"
        doubleLts x    = [x]

myWorkspaces            = clickable . (map xmobarEscape) $ [" 1:\xf269 "," 2:\xf120 "," 3:\xf0e0 ", " 4:\xf07c "," 5:\xf1b6 "," 6:\xf281 "," 7:\xf04b "," 8:\xf167 "," 9:? "]
	where                                                                       
         clickable l = [ "<action=xdotool key alt+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                             (i,ws) <- zip [1..9] l,                                        
                            let n = i ]

myNormalBorderColor  = "#81a1c1" 
myFocusedBorderColor = "#8fbcbb"

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)		-- Terminal
    , ((modm,               xK_Return), spawn "rofi -show run")			-- Run Prompt
    , ((modm .|. shiftMask, xK_c     ), kill)					-- Kill A Window
    , ((modm,               xK_space ), sendMessage NextLayout)			-- Next Layout
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)	-- Reset Layout
    , ((modm,               xK_Tab   ), windows W.focusDown)			-- Swap Between Windows
    , ((modm,               xK_bracketleft), sendMessage Shrink)		-- Shrink Master
    , ((modm,               xK_bracketright), sendMessage Expand)		-- Expand Master
    , ((modm,               xK_s     ), withFocused $ windows . W.sink)		-- Snap Window In Place
    , ((modm              , xK_l ), sendMessage (IncMasterN 1))		        -- Increase Windowcount in master
    , ((modm              , xK_h), sendMessage (IncMasterN (-1)))		-- ^ But Decreases
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))		-- Exit XMonad
    , ((modm              , xK_q), spawn "xmonad --recompile; xmonad --restart")-- Recompile and restart
    
    ]
    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

myLayout = avoidStruts (tiled ||| full)
  where
     full    = gaps [(U,5), (D,5), (L,5), (R,5)] $ Full
     tiled   = gaps [(U,5), (D,5), (L,5), (R,5)] $ smartSpacing 5 $ Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

myEventHook = mempty

myStartupHook = do
	spawnOnce "nitrogen --restore &"
	spawnOnce "xsetroot -cursor_name left_ptr"

main = do
  	xmproc <- spawnPipe "xmobar ~/.xmobarrc.hs"
	xmonad $ docks def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

        keys               = myKeys,
        mouseBindings      = myMouseBindings,
	logHook		   = dynamicLogWithPP $ def {ppOutput = hPutStrLn xmproc
							, ppUrgent          = xmobarColor "red" "yellow"
  							, ppCurrent         = xmobarColor "#4C566A" "#A3BE8C" . wrap "[" "]" -- Current workspace in xmobar
  							, ppVisible         = xmobarColor "#A3BE8C" ""                -- Visible but not current workspace
  							-- \( _ ) -> "" to show no hidden windows
  							, ppHiddenNoWindows = xmobarColor "#BF616A" ""       -- Hidden workspaces (no windows)
  							, ppTitle           = const ""     -- Title of active window in xmobar
  							, ppSep             = "<fc=#D8DEE9> | </fc>"                     -- Separators in xmobar
  							, ppOrder           = \(ws : l : t : ex) -> [ws, l] ++ ex ++ [t]},
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        startupHook        = myStartupHook
    }
		
